#ifndef FONCTIONSANAGRAMME_H_INCLUDED
#define FONCTIONSANAGRAMME_H_INCLUDED
#include <string>

/** fonction anagrammeMot
* role : se charge d'ins�rer chaque lettre dans sa case correspondante
* parametre : mot >> mot envoy� en parametre a chaque fois pour la boucle (differente du mot initial)
                     tab >> tableau devant contenir l'anagramme des mots
                     ligneDebut >> ligne de d�but d'insertion des lettres
                     indiceColonne >> indice de la colonne concern�e par l'insertion des lettres
* retourne : rien
*/
void anagrammeMot(int tailleMotInitial, std::string& mot, char** tab, int ligneDebut, int indiceColonne);

/** fonction operationMot
* role : se charge des operations concernant l'anagramme
* parametre : mot >> mot dont on veut determiner l'anagramme
              tab >> espace (tableau) allou� devant contenir l'anagramme
* retourne : rien
*/
void operationMot(std::string mot, char** tab);

#endif // FONCTIONSANAGRAMME_H_INCLUDED
