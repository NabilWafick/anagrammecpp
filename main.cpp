#include <iostream>
#include <string>
#include "fonctions.h"
#include "fonctionsAnagramme.h"

using namespace std;

int main()
{
   // cout << "ASSALAMOU ANLEYKOUM WA RAHMANTOU LAHI WA BARAKATOUHOU ! "<< endl<<endl<<endl;
    string mot(" ");
    int tailleMot(0);

    cout<<"    SALUT !    "<<endl
             <<"Bienvenue dans le Programme Anagramme ! "<<endl<<endl;
    cout<<"Veuillez entrer un mot : ";
    cin>>mot;
    cout<<endl;

    /// determination de la taille du mot enregistré
    tailleMot=mot.size();

    cout<<"Votre mot est constitue de "<<tailleMot<<" lettres ."<<endl;

    if(repetitionLettre(mot))
    {
        string motLettresRepetees(" "), motLettresNonRepetees(" ");   /// mot qui sera constitué des lettres repetées dans le mot enregistré
        int nombreAnagramme(0);      /// nombre total de mot qu'on peut former à partir du mot enregistré

        lettresRepetees(mot, motLettresRepetees);   /// constitution du mot formé des lettres repétées
        lettresNonRepetees(mot, motLettresNonRepetees); /// constitution du mot formé des lettres repétées
        int tailleMotLettresRepetees(motLettresRepetees.size()), tailleMotLettresNonRepetees(motLettresNonRepetees.size());
        nombreAnagramme=factoriel(tailleMot)/produitFactorielLettresRepetees(mot, motLettresRepetees);  /// détermination de la valeur de nombreAnagramme

        cout<<"Il est constitues de : ";

        int i(0);
        for(i=0; i<tailleMotLettresNonRepetees; i++)
        {
            cout<<nombreOccurenceLettre(mot,motLettresNonRepetees[i])<<" "<<motLettresNonRepetees[i]<<", ";
        }
        for( i=0; i<tailleMotLettresRepetees; i++)
        {
            cout<<nombreOccurenceLettre(mot,motLettresRepetees[i])<<" "<<motLettresRepetees[i];
            if(i != tailleMotLettresRepetees-1)
                cout<<", ";
            else
                cout<<" ."<<endl;
        }

        cout<<"Nombre total de mots qu'on peut former avec les lettres du mot : "<<nombreAnagramme<<endl;

        /// initialisation des dimensions du tableau de l'anagramme
        int nbreLigneTab(factoriel(tailleMot)), nbreColonneTab(tailleMot);

        /// initialisation des tableaux pour l'allocation de memoire
        char **tab(0); /// declaration du pointeur (tableau à deux dimensions)

        tab= new char*[nbreLigneTab];    /// allocation de memoire de pointeurs suivant le nombre de lignes du tableau

        for(int i(0); i<nbreLigneTab; i++)
        {
            tab[i]=new char[nbreColonneTab];   /// allocation de memoire de cases memoires suivant le nombre de colonnes
        }                                                                                               /// pour chaque ligne du tableau

        if(tab != 0)    /// vérification de l'allocation
        {
            cout<<endl<<"Voici les mots qu'on peut former avec ces letrres ! "<<endl<<endl;

            /// vider les espaces mémoires alloués
            viderTab(tab, nbreLigneTab, nbreColonneTab);

            /// réalisation de l'anagramme
            operationMot(mot, tab);

            /// suppression de doublons dans le tableau de l'anagramme
            sansDoublons(tab, nbreLigneTab, nbreColonneTab, nombreAnagramme);

            ///afficher le tableau contenenant l'anagramme sans doublons
            afficherTab(tab, nombreAnagramme, nbreColonneTab);


            char reponse(reponseCorrecte());  /// réponse entrer par l'utilsateur quant à la question de savoir s'il voudrait
                                                                            ///classer les mots de l'anagramme dans l'ordre alphabétique

            if( reponse == 'O' || reponse == 'o' )    /// le classement se fait si l'utilisateur répond Oui par(O ||o)
            {
                cout<<endl<<"Vos mots dans l'ordre alphabetique ! "<<endl<<endl;

                /// classement dans l'ordre alphabétique des mots
                ordreAlphabetique(tab, nombreAnagramme, nbreColonneTab);

                ///afficher le tableau contenenant l'anagramme
                afficherTab(tab, nombreAnagramme, nbreColonneTab);

            }
            else
            {
                cout<<endl<<"Okay ! ";  ///réponse envoyer à l'utilisateur s'il répond Non
            }

            cout<<endl<<"Merci d'avoir utilise le Programme Anagramme ! "<<endl;

        }
        else
        {

            cout<<"ERREUR X :: Memoire Non Allouee ! "<<endl;

        }

        /// libération de la mémoire allouée
        delete tab;
        tab=0;

    }
    else
    {

        /// initialisation des dimensions du tableau devant contenir l'anagramme du mot
        int  nbreLigneTab(factoriel(tailleMot)), nbreColonneTab(tailleMot);

        cout<<endl<<"Il est constitue de : ";

        for( int i(0); i<tailleMot; i++)
        {
            cout<<nombreOccurenceLettre(mot,mot[i])<<" "<<mot[i];
            if(i != tailleMot-1)
                cout<<", ";
            else
                cout<<" ."<<endl;
        }

        cout<<"Nombre total de mots qu'on peut former avec les lettres du mot : "<<nbreLigneTab<<endl;

        /// initialisation du tableau par allocation de memoire
        char** tab(0);  /// declaration du pointeur (tableau à deux dimensions)
        tab=new char*[nbreLigneTab];   /// allocation de memoire de pointeurs suivant le nombre de lignes du tableau

        for(int i(0); i<nbreLigneTab; i++)
        {
            tab[i]=new char[nbreColonneTab];       /// allocation de memoire de cases memoires suivant le nombre de colonnes
        }                                                                   /// pour chaque ligne du tableau

        if(tab==0)    /// vérification de l'allocation
        {
            cout<<"  ERREUR  X :: Memoire Non Allouee"<<endl;
        }
        else
        {
            cout<<endl<<"Voici les mots qu'on peut former avec ces letrres ! "<<endl<<endl;

            /// vider l'espace mémoire alloué
            viderTab(tab, nbreLigneTab, nbreColonneTab);

            /// réalisation de l'anagramme
            operationMot(mot, tab);

            ///afficher le tableau contenenant l'anagramme
            afficherTab(tab, nbreLigneTab, nbreColonneTab);

            char reponse(reponseCorrecte());    /// réponse entrer par l'utilsateur quant à la question de savoir s'il voudrait
                                                                            ///classer les mots de l'anagramme dans l'ordre alphabétique

            if( reponse == 'O' || reponse == 'o' )  /// le classement se fait si l'utilisateur répond Oui par(O ||o)
            {
                cout<<endl<<"Vos mots dans l'ordre alphabetique ! "<<endl<<endl;

                /// classement dans l'ordre alphabétique des mots
                ordreAlphabetique(tab, nbreLigneTab, nbreColonneTab);

                ///afficher le tableau contenenant l'anagramme
                afficherTab(tab, nbreLigneTab, nbreColonneTab);
            }
            else
            {
                cout<<endl<<"Okay ! ";  ///réponse envoyer à l'utilisateur s'il répond Non
            }

            cout<<endl<<"Merci d'avoir utilise le Programme Anagramme ! "<<endl;

            /// libération de la mémoire allouée
            delete tab;
            tab=0;

        }

    }

    cout<<endl<<endl<<endl << "ASSALAMOU ANLEYKOUM WA RAHMANTOU LAHI WA BARAKATOUHOU ! "<<endl;

    return 0;

}
