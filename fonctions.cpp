#include <iostream>
#include "fonctions.h"

using namespace std;


/// fonction factoriel
int factoriel(int nombre)
{
    int fact(1);

    if (nombre==0)
        return 1;
    else
    {
        for (int i(1); i<=nombre; i++)
        {
            fact*=i;
        }
        return fact;
    }
}

/// fonction viderTab
void viderTab( char** tab, int nbreLigne, int nbreColonne)
{
    for(int i(0); i<nbreLigne; i++)
    {
        for(int j(0); j<nbreColonne; j++)
        {
            tab[i][j]=' ';
        }
    }
}

/// fonction indiceLettre
int indiceLettre(string mot, char lettre)
{
    int i(0), tailleMot(mot.size()), indice(0);

    for(i=0; i<tailleMot; i++)
    {
        if(mot[i] == lettre)
        {
            indice=i;
            i=tailleMot;   /// pour forcer l'arr�t pr�coce de la boucle
        }
    }
    return indice;
}

/// fonction repetitionLettre
bool repetitionLettre(string& mot)
{
    unsigned int compteur(0), tailleMot(mot.size());
    char lettre;
    bool repetition=false;

    for(unsigned int i(0); i<tailleMot; i++)
    {
        lettre=mot[i];
        for(unsigned int j(0); j<tailleMot; j++)
        {
            if(lettre == mot[j])
            {
                compteur++;
            }
        }
        if(compteur >1)
        {
            repetition=true;
            i=tailleMot;  /// pour forcer l'arr�t pr�coce de la boucle
        }
        else
            compteur=0;
    }
    return repetition;
}

/// fonction nombreOccurrenceLettre
int nombreOccurenceLettre(string& mot, char lettre)
{
    int compteur(0), tailleMot(mot.size());

    for(int i(0); i<tailleMot; i++)
    {
        if(mot[i] == lettre)
            compteur++;
    }
    return compteur;
}

/// fonction estLettreDansMot
bool estLettreDansMot(string& mot, char lettre)
{
    int tailleMot(mot.size()) ;
    bool presence=false;

    for(int i(0); i<tailleMot; i++)
    {
        if(mot[i] == lettre)
            presence=true;
    }
    return presence;
}

/// fonction lettresRepetees
void lettresRepetees(string& mot, string& motLettresRepetees)
{
    int tailleMot(mot.size());

    for(int i(0); i<tailleMot; i++)
    {
        if(nombreOccurenceLettre(mot, mot[i]) > 1 && estLettreDansMot(motLettresRepetees, mot[i]) == false)
        {
            /// la lettre est ajout�e au mot des lettres rep�t�es si son nombre d'occurence
            motLettresRepetees.push_back(mot[i]);           ///dans le mot principal est sup�rieur � 1 et ne se trouve pas dej� dans
        }                                                                                       ///  le mot des lettres rep�tees
    }
    motLettresRepetees.erase(0,1);
}

void lettresNonRepetees(string& mot, string& motLettresNonRepetees)
{
    int tailleMot(mot.size());

    for(int i(0); i<tailleMot; i++)
    {
        if(nombreOccurenceLettre(mot, mot[i]) == 1)
        {
            /// la lettre est ajout�e au mot des lettres rep�t�es si son nombre d'occurence
            motLettresNonRepetees.push_back(mot[i]);           ///dans le mot principal est �gal � 1
        }
    }
    motLettresNonRepetees.erase(0,1);
}

/// fonction produitFactorielLettresRepetees
int produitFactorielLettresRepetees(string& mot, string& motLettresRepetees)
{
    int tailleMot(motLettresRepetees.size()), produitFactoriel(1);

    for(int i(0); i<tailleMot; i++)
    {
        produitFactoriel*=factoriel(nombreOccurenceLettre(mot,motLettresRepetees[i]));  /// produit des factoriels du nombre d'occurrence de chaque lettre
    }                                                                                                                                                                   ///   devant servir � calculer le nombre d'anagramme
                                                                                        /// Exemple : rire
    return produitFactoriel;                                                            /// nombreAnagramme=!tailleMot/produit( !(nombre de r), !(nombre de i),!(nombre de e))
}

/// fonction afficherTab
void afficherTab(char** tab, int nbreLigne, int nbreColonne)
{
    for(int i(0); i<nbreLigne; i++)    /// boucle por afficher les lignes
    {
        cout<<i+1<<"-";
        for(int j(0); j<nbreColonne; j++)   /// boucle por afficher les colonnes
        {
            cout<<tab[i][j];
        }
        cout<<endl<<endl;
    }
}


/// fonction copieLigne
void copieLigne(char** tab, int nbreColonne, string& mot, int indiceLigne)
{
    for(int j(0); j<nbreColonne; j++)    /// boucle pour copier chaque lettre de la ligne dans mot
    {
        mot.push_back(tab[indiceLigne][j]);    /// push_back(ajoute � chaque fois une lettre)
    }
    mot.erase(0,1);  /// suppression de la premi�re lettre du mot ( espace("") qui �tait initialis� )
}

/// fonction viderLigne
void viderLigne(char** tab, int nbreColonne, int indiceLigne)
{
    for(int j(0); j<nbreColonne; j++)  /// boucle pour remplacer chaque caract�re de la ligne du tableau par espace
    {
        tab[indiceLigne][j]=' ' ;
    }
}

/// fonction estLigneVide
bool estLigneVide(char** tab, int nbreColonne, int indiceLigne)
{
    int compteur(0);
    bool videLigne(false);

    for(int j(0); j<nbreColonne; j++)  /// boucle pour v�rifier le nombre de caract�re de la ligne qui est vide
    {
        if(tab[indiceLigne][j] == ' ')  /// si une case est vide compteur=compteur +1
            compteur++;
    }
    if(compteur == nbreColonne)   /// si � la fin de la boucle, le nombre de case vide est �gal au nombre de colonne alors la ligne set vide et videligne prend la valeur vrai(true)
        videLigne=true;

    return videLigne;
}

/// fonction supprimeDoublons
void supprimeDoublons(char** tab, int nbreLigne, int nbreColonne)
{
    string copie_LignePrincipale(" ");
    string copie_LigneSeconde(" ");

    for(int i(0); i<nbreLigne; i++)   /// boucle parcourant toute les lignes du tableau
    {
        copie_LignePrincipale=" ";  /// mot servant � la copie de la ligne i
        copie_LigneSeconde=" ";   /// mot servant � la copie de la ligne j
        if(estLigneVide(tab, nbreColonne, i) == false)  /// si une ligne est vide, on la copie dans copie_LignePrincipale
        {
            copieLigne(tab, nbreColonne, copie_LignePrincipale, i);
            for(int j(i+1); j<nbreLigne; j++)  ///ligne initiale(i) fix�e, on cherche toutes les lignes j(commen�ant par i+1 jusqu'� la derni�re ligne du tableau)
            {                                                          /// qui ne sont pas vides et qui ont la m�me la configuration que la ligne(i) fix�e( sont identiques(doublons de mots))
                copie_LigneSeconde=" ";
                if(estLigneVide(tab, nbreColonne, j) == false)
                {
                    copieLigne(tab, nbreColonne, copie_LigneSeconde, j);
                    if(copie_LignePrincipale == copie_LigneSeconde)  /// si une ligne j est identique � la ligne i fix�e, elle est vid�e
                        viderLigne(tab, nbreColonne, j);
                }
            }
        }
    }
}

/// fonction presenceDeLigneVide
bool presenceDeLigneVide(char** tab, int nbreColonne, int nombreAnagramme)
{
    bool presence(false);

    for(int i(0); i<nombreAnagramme; i++)  /// boucle pour v�rifier s'il y a de ligne vide entre ligne(0) et ligne(nombreAnagramme)
    {
        if(estLigneVide(tab, nbreColonne, i))
        {
            presence=true;  /// si il y a de lignes de lignes vides dans l'intervalle de lignes fix�, presense prende la valeur vrai
            i=nombreAnagramme; /// la boucle est arr�t�e d�s qu'une ligne vide est identifi�e
        }
    }
    return presence;
}

/// fonction copieMotDansTab
void copieMotDansTab(char** tab, int nbreColonne, string& mot, int indiceLigne)
{

    for(int i(0); i<nbreColonne; i++)  /// boucle pour copier chaque lettre du mot dans le tableau
    {
        tab[indiceLigne][i]=mot[i];
    }
}

/// fonction regrouperMotsRestants
void regrouperMotsRestants(char** tab, int nbreLigne, int nbreColonne, int nombreAnagramme)
{
    string ligne(" ");

    while(presenceDeLigneVide(tab,nbreColonne, nombreAnagramme) == true)   /// tant qu'il y a de ligne(s) vide(s) entre ligne(0) et ligne(nombreAnagrammme)
    {
        for(int i(1); i<nbreLigne; i++) /// de la deuxi�me ligne � la derni�re ligne du tableau (car la premi�re ligne ne peut �tre vide)
        {
            ligne=" ";
            if(estLigneVide(tab, nbreColonne, i) == false && estLigneVide(tab, nbreColonne, i-1) == true) /// si une ligne est non vide et la p�c�dente est vide
            {
                copieLigne(tab, nbreColonne, ligne, i);     /// la ligne non vide est copi� dans ligne
                copieMotDansTab(tab, nbreColonne, ligne, i-1);      /// ligne est copi� dans le tableau au niveau de la pr�c�dente (ligne vide)
                viderLigne(tab, nbreColonne, i); /// la ligne initialement non vide est vid�e
            }
        }
    }
}

/// fonction sansDoublons
void sansDoublons(char** tab, int nbreLigne, int nbreColonne, int nombreAnagramme)
{
    supprimeDoublons(tab, nbreLigne, nbreColonne);   /// appel de la fonction supprimant les doublons
    regrouperMotsRestants(tab, nbreLigne, nbreColonne, nombreAnagramme);  /// appel de  la fonction regroupant les mots restants du tableau ap�s suppressiion des doublons
}

/// fonction ordreAlphabetique
void ordreAlphabetique(char** tab, int nbreLigne, int nbreColonne)
{
    string copie_LignePrincipale(" "), copie_LigneSeconde(" "), tmp(" ");  // chaines de caract�res servant � la copie des lignes du tableau
    /// tri � bulles des mots
    for(int i(1); i<=nbreLigne-1; i++)
    {
        copie_LignePrincipale=" ";
        copie_LigneSeconde=" ";
         copieLigne(tab, nbreColonne, copie_LignePrincipale, i);
        for(int j(0); j<nbreLigne-1; j++)
        {
            copie_LigneSeconde=" ";
            copieLigne(tab, nbreColonne, copie_LigneSeconde, j);
            if(copie_LignePrincipale < copie_LigneSeconde)
            {
                tmp=copie_LignePrincipale;
                copie_LignePrincipale=copie_LigneSeconde;
                copie_LigneSeconde=tmp;
                copieMotDansTab(tab, nbreColonne, copie_LignePrincipale, i);
                copieMotDansTab(tab, nbreColonne, copie_LigneSeconde, j);
            }
        }
    }

}

/// fonction reponseCorrecte
char reponseCorrecte( )
{
    char reponse('N');

     cout<<endl<<"Voulez-vous ranger les mots dans l'ordre alphabetique ? "<<endl;
     cout<<"Veuillez repondre par (O/o) pour Oui ou (N/n) pour Non : ";

    cin>>reponse;

    while((reponse != 'O' && reponse != 'o') && (reponse != 'N' && reponse != 'n'))
    {
        cout<<"Veuillez entrer une reponse valide s'il vous plait ! "<<endl
        <<"Veuillez entrer votre reponse : ";
        cin>>reponse;
    }

    return reponse;
}
