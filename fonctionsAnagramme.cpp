#include <iostream>
#include <string>
#include "fonctions.h"
#include "fonctionsAnagramme.h"

using namespace std;

/// fonction anagrammeMot (Moteur duProgramme Anagramme)
void anagrammeMot(int tailleMotInitial, string& mot_principal, char** tab, int ligneDebut, int indiceColonne)
{
    int i(0), l(0), tailleMot(mot_principal.size());  /// determination de la chaine entr�e en param�tre
    int ligne (factoriel(tailleMot-1)); /// calcul du nombre de lignes sur lesquelles chaque lettre sera ins�r�e
    int line(ligneDebut);   /// stockage de la ligne de d�but pour chaque lettre
    string mot(mot_principal), copie_mot(mot);      /// mot qui sera plutard utilis� dans la fonction anagramme incluse,
    /// copie_mot utilis� apr�s une operation d'anagramme interne sevant � ramener
    /// la chaine (string) r�duite avant l'anagramme � sa taille initiale avant sa r�duction
    int lettreIndice(0); /// indice de la lettre ins�r�e dans la colonne i et qui ne doit pas �tre envoy�e dans la colonne i+1

    for(i=0; i<tailleMot; i++)      /// boucle pour passer sur chaque lettre du mot
    {
        ligneDebut=line; /// pour le debut de l'insertion de la colonne suivante dans l'appel de la fonction anagramme recursive

        for(l=line; l<(line+ligne); l++)    /// boucle d'insertion de la lettredans lescases correspondantes
        {
            tab[l][indiceColonne]=mot[i];   /// insertion de chaque lettre dans sa case
        }
        if(indiceColonne != (tailleMotInitial-1))       /// on ne depasse pas la derni�re colonne (pas de reduction de mot car
            ///on n'appelle pas la fonction anagramme dans la derni�re colonne)
        {

            lettreIndice=indiceLettre(mot, mot[i]);
            mot.erase(lettreIndice,1);
            anagrammeMot(tailleMotInitial, mot, tab, ligneDebut, (indiceColonne+1));  /// appel r�cursif de la fonction anagramme
            /// pour la colonne suivante
            mot=copie_mot;   /// pour continuer avec les lettres restantes du mot
        }
        line=l;   /// conservation du num�ro de la derni�re ligne [+1] (+1 d� � la derni�re incr�mentation de la boucle for qui
        /// n'agira sur l'insertion de lettre "la condition d'insertion n'�tant pas v�rifi�e") sur laquelle est inscrite
        /// la lettre pr�c�dente pour pouvoir continuer l'insertion sur la ligne suivante de la prochaine lettre
    }
}


/// fonction operationMot
void operationMot(string mot, char** tab)
{
    int tailleMot(mot.size());

    anagrammeMot(tailleMot, mot, tab,0,0);  /// appel principal de la fonction anagramme
}
