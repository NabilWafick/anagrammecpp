#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED
#include <string>

/** fonction factoriel
* role : se charge de determiner le factoriel d'un nombre
* parametre : nombre >> nombre dont on veut determiner le factoriel
* retourne : le factoriel du nombre qui lui est envoy�
*/
int factoriel(int  nombre);

/** foction viderTab
* role : se charger vider l'espace memoire allou�
* parametre : tab >> espace (tableau) allou� devant contenir l'anagramme
                   nbreLigne >> nombre de ligne du tableau
                   nbreColonne >> nombre de colonne du tableau
* retourne : rien
*/
void viderTab( char** tab, int  nbreLigne, int nbreColonne);

/** fonction indiceLettre
* role : se charge d'identifier l'indice de la lettre dans le mot en  envoy� param�tre
* parametre : mot >> mot comportant la lettre
                     lettre >> lettre dont on veut chercher
* retourne : l'indice de la lettre
*/
int indiceLettre(std::string mot, char lettre);

/** fonction repetitionLettre
* role : se charge de v�rifier s'il ya des lettres rep�t�es dans le mot entr� en param�tre
* parametre : mot >> mot dont on v�rifier la r�p�tition des lettres
* retourne : un bool (true si il y a r�p�tion de lettres, false sinon)
*/
bool repetitionLettre(std::string& mot);

/** fonction nombreOccurenceLettre
* role : se charge de compter le nombre de r�p�tition d'une lettre dans le mot entr� en param�tre
* parametre : mot >> le mot dans lequel on veut chercher le nombre d'occurence de chaque lettre
                    lettre >> lettre dont on veut chercher le nombre d'occurrence
* retourne : le nombre d'occurrence de la lettre entr� en param�tre
*/
int nombreOccurenceLettre(std::string& mot, char lettre);

/** fonction estLettreDansMot
* role : se charge de v�rifier si la lettre pass�e en param�tre est incluse dans le mot qui l'est aussi
* parametre : mot >> mot dans lequel on veut v�rifier la pr�sence de la lettre
                    lettre >> lettre dont on veut v�rifier la pr�sence dans le mot pass� en param�tre
* retourne : un bool (true si la lettre est incluse dans le mot, false sinon)
*/
bool estLettreDansMot(std::string& mot, char lettre);

/** lettresRepetees
* role : se charge de constituer un mot (seconde cha�ne de caract�res entr�e en param�tre) avec les lettres rep�t�es
         dans le premier mot entr� en param�tre
* parametre : mot >> le mot dans lequel sera recherch�e les lettres rep�t�es
                     motLettresRepetees >> mot qui sera constitu� des lettres rep�t�es dans le premier entr� en param�tre
* retourne : rien
*/
void lettresRepetees(std::string& mot, std::string& motLettresRepetees);

/** lettresNonRepetees
* role : se charge de constituer un mot (seconde cha�ne de caract�res entr�e en param�tre) avec les lettres non rep�t�es
         dans le premier mot entr� en param�tre
* parametre : mot >> le mot dans lequel sera recherch�e les lettres non rep�t�es
                     motLettresNonRepetees >> mot qui sera constitu� des lettres non rep�t�es
* retourne : rien
*/
void lettresNonRepetees(std::string& mot, std::string& motLettresNonRepetees);

/** fonction produitFactorielLettresRepetees
* role : se charge de calculer le factoriel du nombre d'occurence de chaque lettre puis fais le produit des factoriels calcul�s
* parametre : mot >> mot dans lequel est tir� les lettres (rep�t�es)
                    motLettresRepetees >> mot constitu� � partir des lettres rep�t�es dans le mot principal
* retourne : le produit des factoriels du nombre d'occurence des lettres rep�t�es
*/
int produitFactorielLettresRepetees(std::string& mot, std::string& motLettresRepetees);

/** fonction afficherTab
* role : affiche le tableau contenant l'anagramme du mot
* parametre : tab >> espace (tableau) allou� devant contenir l'anagramme
                   nbreLigne >> nombre de ligne du tableau
                   nbreColonne >> nombre de colonne du tableau
* retourne : rien
*/
void afficherTab( char** tab, int  nbreLigne, int  nbreColonne);


/** fonction copieLigne
* role : se charge de copier une ligne du tableau pass� en param�tre dans le mot qui l'est aussi
* parametre : tab >> tableau duquel sera copier le mot
                    nbreColonne >> nombres de colonnes du tableau (taille du mot enregistr�)
                    mot >> mot � constituer � partir d'une ligne du tableau
                    indiceLigne >> ligne � copier
* retourne : rien
*/
void copieLigne(char** tab, int nbreColonne, std::string& mot, int indiceLigne);

/** fonction viderLigne
* role : se charge de trouver la premi�re ligne vide dans le tableau
* parametre : tab >> tableau dans lequel on veut vider une ligne
                     nbreColonne >> nombre de colonnes du tableau (taille du mot enregistr�
                     indiceLigne >> indice de la ligne � vider
* retourne : rien
 */
void viderLigne(char** tab, int nbreColonne, int indiceLigne);

/** fonction estLigneVide
* role : se charge de v�rifier si une ligne du tableau est vide
* parametre : tab >> tableau dans lequel on veut v�rifier l'�tat de la ligne
                    nbreColonne >> nombre de colonnes du tableau (taille du mot enregistr�
                    indiceLigne >> indice de la ligne dont on veut v�rifier l'�tat
* retourne : bool(true si la ligne est vide, false sinon)
*/
bool estLigneVide(char** tab, int nbreColonne, int indiceLigne);

/** fonction supprimeDoublons
* role : se charge de supprimer les doublons des mots dans le tableau pass� en param�tre(tableau de l'anagramme)
* parametre : tab >> tableau dans lequel on veut supprimer les doublons
                   nbreLigne >> nombre de lignes du tableau
                   nbreColonne >> nombres de colonnes du tableau (taille du mot enregistr�)
* retourne : rien
*/
void supprimeDoublons(char** tab, int nbreLigne, int nbreColonne);

/** fonction presenceDeLigneVide
* role : se charge de v�rifier s'il y a de ligne vide ou pas dans un intervale de lignes (0 et nombreAnagramme-1) ligne
* parametre : tab >> tableau dans lequel se fera la v�rification
                    nbreColonne >> nombres de colonnes du tableau (taille du mot enregistr�)
                    nombreAnagramme >> nombre total de mot qu'on peut former � partir du mot enregistr�
* retourne : bool(true s'il n'y a pas de ligne vide entre (0 et nombreAnagramme-1) ligne et false)
 */
bool presenceDeLigneVide(char** tab, int nbreColonne, int nombreAnagramme);

/** fonction copieMotDansTab
* role : se charge de copier un mot dans le tableau
* parametre : tab >> tableau dans lequel on veut copier le mot
                     nbreColonne >> nombres de colonnes du tableau (taille du mot enregistr�)
                     mot >> mot � copier dans le tableau
                     indiceLigne >> ligne � remplir
* retourne : rien
 */
void copieMotDansTab(char** tab, int nbreColonne, std::string& mot, int indiceLigne);

/** fonction regrouperMotsRestants
* role : se charge de regrouper le reste des mots dans le tableau apr�s suppression des doublons
* parametre : tab >> tableau dans lequel se fera le regroupement
                   nbreLigne >> nombre de lignes du tableau
                   nbreColonne >> nombres de colonnes du tableau (taille du mot enregistr�)
                   nombreAnagramme >> nombre total de mot qu'on peut former � partir du mot enregistr�
* retourne : rien
*/
void regrouperMotsRestants(char** tab, int nbreLigne, int nbreColonne);/*, int nombreAnagramme/);*/

/** fonction sansDoublons
* role : se charge de supprimer les doublons et de regrouper les mots restants dans le tableau
* parametre : tab >>  tableau contenant l'anagramme
                   nbreLigne >> nombre de lignes du tableau
                   nbreColonne >> nombres de colonnes du tableau
                   nombreAnagramme >> nombre total de mot qu'on peut former � partir du mot enregistr�
* retourne : rien
*/
void sansDoublons(char** tab, int nbreLigne, int nbreColonne, int nombreAnagramme);

/** fonction ordreAlphabetique
* role : se charge de ranger les lignes du tableau(mots) dans l'ordre alphabetique
* parametre : tab >>  tableau contenant l'anagramme
                   nbreLigne >> nombre de lignes du tableau
                   nbreColonne >> nombres de colonnes du tableau
* retourne : rien
*/
void ordreAlphabetique(char** tab, int nbreLigne, int nbreColonne);

/** fonction reponseCorrecte
* role : se charge de recueillir une r�ponse correcte de la part de l'utilisateur
* parametre :
* retourne : char(O pour Oui, N pour Non)
*/
char reponseCorrecte( );

#endif // FONCTIONS_H_INCLUDED
